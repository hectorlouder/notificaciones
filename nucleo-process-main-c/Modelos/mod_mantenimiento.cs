﻿namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.mod_mantenimiento")]
    public partial class mod_mantenimiento
    {
        [Key]
        public int idMantenimiento { get; set; }

        public int idGPS { get; set; }

        [StringLength(45)]
        public string descripcionMantenimiento { get; set; }

        public bool? boolAlerta { get; set; }

        public int? kilometraje { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fecha { get; set; }

        public decimal? kilometrajeA { get; set; }

        public virtual gps gps { get; set; }
    }
}
