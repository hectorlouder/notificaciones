namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.usuario")]
    public partial class usuario
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public usuario()
        {
            notificacion = new HashSet<notificacion>();
            notificacion_ios = new HashSet<notificacion_ios>();
            notificacion_modulo = new HashSet<notificacion_modulo>();
            notificaciones_usuario = new HashSet<notificaciones_usuario>();
            usuario_gps = new HashSet<usuario_gps>();
        }

        [Key]
        public int idUsuario { get; set; }

        public int idEmpresa { get; set; }

        [Required]
        [StringLength(60)]
        public string nombre { get; set; }

        [StringLength(60)]
        public string apellidoP { get; set; }

        [StringLength(60)]
        public string apellidoM { get; set; }

        [StringLength(250)]
        public string email { get; set; }

        [StringLength(45)]
        public string notas { get; set; }

        [Required]
        [StringLength(45)]
        public string pseudonimo { get; set; }

        [Required]
        [StringLength(60)]
        public string pass { get; set; }

        public DateTime? fechaInicio { get; set; }

        public DateTime? fechaFinal { get; set; }

        public byte[] imagen { get; set; }

        public int activo { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaNacimiento { get; set; }

        [StringLength(45)]
        public string palabra_secreta { get; set; }

        [StringLength(45)]
        public string telefonoEmergencias { get; set; }

        public int? tipo { get; set; }

        public virtual empresa empresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notificacion> notificacion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notificacion_ios> notificacion_ios { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notificacion_modulo> notificacion_modulo { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notificaciones_usuario> notificaciones_usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usuario_gps> usuario_gps { get; set; }
    }
}
