namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.gps")]
    public partial class gps
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public gps()
        {
            adc1 = new HashSet<adc>();
            contacto_gps = new HashSet<contacto_gps>();
            empresa_gps = new HashSet<empresa_gps>();
            evento1 = new HashSet<evento>();
            grupos_gps = new HashSet<grupos_gps>();
            usuario_gps = new HashSet<usuario_gps>();
            datos_vehiculo = new HashSet<datos_vehiculo>();
        }

        [Key]
        public int idGPS { get; set; }

        [Required]
        [StringLength(45)]
        public string imeiGPS { get; set; }

        [StringLength(45)]
        public string nombreGPS { get; set; }

        public DateTime? fechaPosicion { get; set; }

        public DateTime? fechaRecepcion { get; set; }

        [StringLength(20)]
        public string direccion { get; set; }

        public Double latitud { get; set; }

        public Double longitud { get; set; }

        public decimal? velocidad { get; set; }

        [StringLength(45)]
        public string estatusEntradas { get; set; }

        [StringLength(10)]
        public string numeroChip { get; set; }

        public int? tipo { get; set; }

        public int? alertas { get; set; }

        [StringLength(5)]
        public string panicos { get; set; }

        [NotMapped]
        public int[] panicosArray {
            get{
                int[] panicos = new int[3];
                if (!string.IsNullOrEmpty(this.panicos)) panicos = Array.ConvertAll(this.panicos.ToCharArray(), a => (int)Char.GetNumericValue(a));
                return panicos;
            }
        }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string infoAlertas { get; set; }

        public decimal? velocidadMaxima { get; set; }

        public int? diasOperacion { get; set; }

        [StringLength(70)]
        public string inicioOperacion { get; set; }

        [StringLength(70)]
        public string finOperacion { get; set; }

        [StringLength(3)]
        public string evento { get; set; }

        public DateTime? fechaUltimoEncendido { get; set; }

        public double acumuladorDistancia { get; set; }

        public decimal? ADC { get; set; }

        public sbyte? zonaHoraria { get; set; }

        public int? estatusMotor { get; set; }

        [StringLength(10)]
        public string versionSW { get; set; }

        [StringLength(8)]
        public string pass3 { get; set; }

        public int? estatus { get; set; }

        [StringLength(45)]
        public string ipGPS { get; set; }

        [StringLength(45)]
        public string puertoGPS { get; set; }

        public int? modelo { get; set; }

        [StringLength(200)]
        public string direccionGeo { get; set; }

        [StringLength(100)]
        public string imeiChip { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<adc> adc1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacto_gps> contacto_gps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<empresa_gps> empresa_gps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<evento> evento1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<grupos_gps> grupos_gps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usuario_gps> usuario_gps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<datos_vehiculo> datos_vehiculo { get; set; }
    }
}
