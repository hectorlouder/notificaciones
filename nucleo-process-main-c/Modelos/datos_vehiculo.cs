namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.datos_vehiculo")]
    public partial class datos_vehiculo
    {
        [Key]
        public int idDatos { get; set; }

        public int idGPS { get; set; }

        [StringLength(45)]
        public string marca { get; set; }

        public int? anio { get; set; }

        [StringLength(45)]
        public string modelo { get; set; }

        [StringLength(45)]
        public string color { get; set; }

        [StringLength(200)]
        public string descripcion { get; set; }

        public int? odometro { get; set; }

        [StringLength(10)]
        public string placas { get; set; }

        public int idAseguradora { get; set; }

        [StringLength(45)]
        public string poliza { get; set; }

        public sbyte? arrendado { get; set; }

        public byte[] imagen { get; set; }
    }
}
