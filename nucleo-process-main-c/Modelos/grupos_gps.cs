namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.grupos_gps")]
    public partial class grupos_gps
    {
        [Key]
        public int idRGG { get; set; }

        public int idGrupo { get; set; }

        public int idGPS { get; set; }

        public virtual gps gps { get; set; }

        public virtual grupos grupos { get; set; }
    }
}
