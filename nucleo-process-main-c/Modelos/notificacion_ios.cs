namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.notificacion_ios")]
    public partial class notificacion_ios
    {
        [Key]
        public int idNIOS { get; set; }

        [StringLength(250)]
        public string datos { get; set; }

        public DateTime? fecha { get; set; }

        [StringLength(150)]
        public string gcm { get; set; }

        public int idUsuario { get; set; }

        public virtual usuario usuario { get; set; }
    }
}
