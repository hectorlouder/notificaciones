﻿namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.adc")]
    public partial class adc
    {
        [Key]
        public int idADC { get; set; }

        public int? idGPS { get; set; }

        public decimal? ADC1 { get; set; }

        public decimal? ADC2 { get; set; }

        [StringLength(5)]
        public string unidadADC1 { get; set; }

        [StringLength(5)]
        public string unidadADC2 { get; set; }

        [StringLength(100)]
        public string condicionADC1 { get; set; }

        [StringLength(100)]
        public string condicionADC2 { get; set; }

        public decimal? ADC1Convertido { get; set; }

        public decimal? ADC2Convertido { get; set; }

        public virtual gps gps { get; set; }
    }
}
