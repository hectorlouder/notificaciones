﻿namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.pendientes_cenavit")]
    public partial class pendientes_cenavit
    {
        [Key]
        public int idPendientes { get; set; }

        [Required]
        [StringLength(400)]
        public string query { get; set; }

        public int estatus { get; set; }

        public DateTime? fechaC { get; set; }

        public DateTime? fechaU { get; set; }
    }
}
