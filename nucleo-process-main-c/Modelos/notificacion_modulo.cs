namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.notificacion_modulo")]
    public partial class notificacion_modulo
    {
        [Key]
        public int idRNM { get; set; }

        public int idModulo { get; set; }

        public int idUsuario { get; set; }

        [StringLength(10)]
        public string via { get; set; }

        [StringLength(10)]
        public string claveModulo { get; set; }

        public virtual usuario usuario { get; set; }
    }
}
