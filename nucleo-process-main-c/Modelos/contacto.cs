namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.contacto")]
    public partial class contacto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public contacto()
        {
            contacto_gps = new HashSet<contacto_gps>();
        }

        [Key]
        public int idContacto { get; set; }

        public int idEmpresa { get; set; }

        [StringLength(50)]
        public string nombre { get; set; }

        [StringLength(15)]
        public string telCasa { get; set; }

        [StringLength(15)]
        public string telCelular { get; set; }

        [StringLength(200)]
        public string email { get; set; }

        public int? activo { get; set; }

        [StringLength(250)]
        public string notas { get; set; }

        [StringLength(45)]
        public string palabraSecreta { get; set; }

        [Column(TypeName = "date")]
        public DateTime? fechaNacimiento { get; set; }

        [StringLength(45)]
        public string notificaciones { get; set; }

        public virtual empresa empresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacto_gps> contacto_gps { get; set; }
    }
}
