namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.usuario_gps")]
    public partial class usuario_gps
    {
        [Key]
        public int idRUG { get; set; }

        public int idUsuario { get; set; }

        public int idGPS { get; set; }

        public virtual gps gps { get; set; }

        public virtual usuario usuario { get; set; }
    }
}
