namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.empresa_gps")]
    public partial class empresa_gps
    {
        [Key]
        public int idREG { get; set; }

        public int idEmpresa { get; set; }

        public int idGPS { get; set; }

        public virtual empresa empresa { get; set; }

        public virtual gps gps { get; set; }
    }
}
