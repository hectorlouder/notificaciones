namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.cola_gps")]
    public partial class cola_gps
    {
        [Key]
        public int idCola { get; set; }

        public int idGPS { get; set; }

        public DateTime fecha { get; set; }

        [StringLength(10)]
        public string versionSW { get; set; }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string cadena { get; set; }

        [NotMapped]
        public double SafeWord {
            get{ return Convert.ToDouble(this.cadena.Split(',')[0]); }
        }

        [NotMapped]
        public DateTime cadenaFecha{
            get {
                System.Globalization.CultureInfo prov = default(System.Globalization.CultureInfo);
                prov = System.Globalization.CultureInfo.InvariantCulture;

                var h = this.cadena.Split(',')[1].Split('.')[0];
                var f = this.cadena.Split(',')[9];

                DateTime fecha;

                if (this.versionSW.Substring(0, 2) == "SA" || this.versionSW.StartsWith("MTK")) fecha = DateTime.ParseExact(f + " " + h, "yyyyMMdd HH:mm:ss", prov);
                else fecha = DateTime.ParseExact(f + " " + h, "ddMMyy HHmmss", prov);

                fecha.AddHours(Properties.Settings.Default.HusoHorario);

                return fecha;
            }
        }

        [NotMapped]
        public string letraTrasmision{
            get{ return this.cadena.Split(',')[2]; }
        }

        [NotMapped]
        public Double latitud {
            get {
                Double l = Convert.ToDouble(this.cadena.Split(',')[3]);

                //Valor de la longitud
                if (this.versionSW.Substring(0, 2) != "SA" && !this.versionSW.StartsWith("MTK")) l /= 100;

                //Ver letra de direccion
                if (DireccionLatitud != "N") l = -l;

                return l;
            }
        }

        [NotMapped]
        public string DireccionLatitud {
            get { return this.cadena.Split(',')[4]; }
        }

        [NotMapped]
        public Double longitud{
            get {
                Double l = Convert.ToDouble(this.cadena.Split(',')[5]);
                //Entre 100
                if (this.versionSW.Substring(0, 2) != "SA" && !this.versionSW.StartsWith("MTK") ) l /= 100;
                
                //Ver letra de direccion
                if (Direccionlongitud != "E" && this.versionSW != "RPT") l = -l;

                return l;
            }
        }

        [NotMapped]
        public string Direccionlongitud{
            get { return this.cadena.Split(',')[6]; }
        }

        [NotMapped]
        public string coordenadas
        {
            get { return this.latitud + "," + this.longitud; }
        }

        [NotMapped]
        public Double velocidad{
            get {
                Double velocidad = Convert.ToDouble(this.cadena.Split(',')[7]);

                //Valor base para la conversion de nudos a km = 1.852
                if (this.versionSW == "Skypt"){
                    velocidad = velocidad * 1.852;
                }else velocidad = velocidad * 3.6;

                if (!this.statusMotor){
                    if (velocidad < 10) velocidad = 0;
                }

                return velocidad;
            }
        }

        [NotMapped]
        public Double angulo{
            get { return Convert.ToDouble(this.cadena.Split(',')[8]); }
        }

        [StringLength(10)]
        public string statusEntradas { get; set; }

        [NotMapped]
        public int[] statusGPS {
            get {
                int[] binary = new int[7];
                if ( this.statusEntradas.StartsWith("IM") ){
                    var numero = Convert.ToInt64(this.statusEntradas.Substring(3).Trim(), 10);
                    binary = Array.ConvertAll(Convert.ToString(numero, 2).ToCharArray(), a => (int)Char.GetNumericValue(a));
                    Array.Reverse(binary);
                }

                return binary;
            }
        }

        [NotMapped]
        public bool statusMotor {
            get {  return (this.statusGPS[0] == 1)? true : false; }
        }

        [StringLength(3)]
        public string eventos { get; set; }

        public int? voltaje { get; set; }

        public int? temperatura { get; set; }

        public int? ADC { get; set; }

        public int? estatusProcesamiento { get; set; }

        public sbyte? tipo { get; set; }

        public virtual gps gps { get; set; }
    }
}
