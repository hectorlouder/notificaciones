namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.contacto_gps")]
    public partial class contacto_gps
    {
        [Key]
        public int idRCG { get; set; }

        public int idContacto { get; set; }

        public int idGPS { get; set; }

        public virtual contacto contacto { get; set; }

        public virtual gps gps { get; set; }
    }
}
