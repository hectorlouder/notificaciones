namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.evento")]
    public partial class evento
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public evento()
        {
            notificacion = new HashSet<notificacion>();
        }

        [Key]
        public int idEvento { get; set; }

        public int idClave { get; set; }

        public int idGPS { get; set; }

        public DateTime? fecha { get; set; }

        public bool estatus { get; set; }

        [StringLength(400)]
        public string descripcion { get; set; }

        [StringLength(100)]
        public string coordenadas { get; set; }
        [NotMapped]
        public double latitud {
            get{
                return Convert.ToDouble(this.coordenadas.Split(',')[0]);
            }
        }
        [NotMapped]
        public double longitud{
            get{
                return Convert.ToDouble(this.coordenadas.Split(',')[1]);
            }
        }

        public int? extra { get; set; }

        [StringLength(45)]
        public string titulo { get; set; }

        [NotMapped]
        public string clave_completa{
            get{
                string completa;
                if (this.extra != 0){
                    completa = this.claves.clave + "-" + this.extra;
                }
                else completa = Convert.ToString(this.claves.clave);

                return completa;
            }
        }

        public virtual claves claves { get; set; }

        public virtual gps gps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<notificacion> notificacion { get; set; }
    }
}
