namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Globalization;
    [Table("gps.grupos")]
    public partial class grupos
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public grupos()
        {
            grupos_gps = new HashSet<grupos_gps>();
        }

        [Key]
        public int idGrupo { get; set; }

        [StringLength(100)]
        public string nombreGrupo { get; set; }

        public int idEmpresa { get; set; }

        public double? velocidadMaxima { get; set; }

        public int? diasOperacion { get; set; }

        [NotMapped]
        public int[] adiasOperacion {
            get {
                int[] binary = new int[7];
                var numero = Convert.ToInt32(this.diasOperacion);
                binary = Array.ConvertAll(Convert.ToString(numero, 2).ToCharArray(), a => (int)Char.GetNumericValue(a));
                Array.Reverse(binary);

                return binary;
            }
        }

        [StringLength(70)]
        public string inicioOperacion { get; set; }

        [NotMapped]
        public DateTime[] ainicioOperacion {
            get {
                DateTime[] horas = new DateTime[7];
                horas = Array.ConvertAll(this.inicioOperacion.Split(','), a => DateTime.ParseExact(a, "HH:mm:ss", CultureInfo.InvariantCulture));
                return horas;
            }
        }

        [StringLength(70)]
        public string finOperacion { get; set; }

        [NotMapped]
        public DateTime[] afinOperacion{
            get{
                DateTime[] horas = new DateTime[7];
                horas = Array.ConvertAll(this.finOperacion.Split(','), a => DateTime.ParseExact(a, "HH:mm:ss", CultureInfo.InvariantCulture));
                return horas;
            }
        }

        [Column(TypeName = "text")]
        [StringLength(65535)]
        public string descripcionGrupo { get; set; }

        public virtual empresa empresa { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<grupos_gps> grupos_gps { get; set; }
    }
}
