namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.notificacion")]
    public partial class notificacion
    {
        [Key]
        public int idNotificacion { get; set; }

        public int idEvento { get; set; }

        public int idUsuario { get; set; }

        public DateTime? fecha { get; set; }

        public bool? estatus { get; set; }

        public int? mostrado { get; set; }

        public virtual evento evento { get; set; }

        public virtual usuario usuario { get; set; }
    }
}
