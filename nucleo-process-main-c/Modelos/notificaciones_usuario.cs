namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.notificaciones_usuario")]
    public partial class notificaciones_usuario
    {
        [Key]
        public int idRNU { get; set; }

        public int idUsuario { get; set; }

        [StringLength(10)]
        public string correo { get; set; }

        [StringLength(10)]
        public string movil { get; set; }

        [StringLength(10)]
        public string web { get; set; }

        public bool? estatus { get; set; }

        public virtual usuario usuario { get; set; }
    }
}
