namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    
    [Table("gps.usuario_disp")]
    public partial class usuario_disp
    {
        [Key]
        public int idReg { get; set; }

        public int idUsuario { get; set; }

        [StringLength(200)]
        public string gcm { get; set; }

        public int plataforma { get; set; }

        [StringLength(100)]
        public string imei { get; set; }

        public DateTime fechaRegistro { get; set; }

        public virtual usuario usuario { get; set; }
    }
}
