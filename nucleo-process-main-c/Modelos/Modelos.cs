namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Modelos : DbContext
    {
        public Modelos() : base("name=Model1") { }

        public virtual DbSet<adc> adc { get; set; }
        public virtual DbSet<claves> claves { get; set; }
        public virtual DbSet<contacto> contacto { get; set; }
        public virtual DbSet<contacto_gps> contacto_gps { get; set; }
        public virtual DbSet<datos_vehiculo> datos_vehiculo { get; set; }
        public virtual DbSet<empresa> empresa { get; set; }
        public virtual DbSet<empresa_gps> empresa_gps { get; set; }
        public virtual DbSet<evento> evento { get; set; }
        public virtual DbSet<gps> gps { get; set; }
        public virtual DbSet<grupos> grupos { get; set; }
        public virtual DbSet<grupos_gps> grupos_gps { get; set; }
        public virtual DbSet<notificacion> notificacion { get; set; }
        public virtual DbSet<notificacion_ios> notificacion_ios { get; set; }
        public virtual DbSet<notificacion_modulo> notificacion_modulo { get; set; }
        public virtual DbSet<notificaciones_usuario> notificaciones_usuario { get; set; }
        public virtual DbSet<usuario> usuario { get; set; }
        public virtual DbSet<usuario_gps> usuario_gps { get; set; }
        public virtual DbSet<usuario_disp> usuario_disp { get; set; }
        public virtual DbSet<key_maps> key_maps { get; set; }
        public virtual DbSet<cola_gps> cola_gps { get; set; }
        public virtual DbSet<pendientes_cenavit> pendientes_cenavit { get; set; }
        public virtual DbSet<mod_mantenimiento> mod_mantenimiento { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<adc>()
                .Property(e => e.unidadADC1)
                .IsUnicode(false);

            modelBuilder.Entity<adc>()
                .Property(e => e.unidadADC2)
                .IsUnicode(false);

            modelBuilder.Entity<adc>()
                .Property(e => e.condicionADC1)
                .IsUnicode(false);

            modelBuilder.Entity<adc>()
                .Property(e => e.condicionADC2)
                .IsUnicode(false);

            modelBuilder.Entity<claves>()
                .Property(e => e.clave)
                .IsUnicode(false);

            modelBuilder.Entity<claves>()
                .Property(e => e.titulo)
                .IsUnicode(false);

            modelBuilder.Entity<contacto>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<contacto>()
                .Property(e => e.telCasa)
                .IsUnicode(false);

            modelBuilder.Entity<contacto>()
                .Property(e => e.telCelular)
                .IsUnicode(false);

            modelBuilder.Entity<contacto>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<contacto>()
                .Property(e => e.notas)
                .IsUnicode(false);

            modelBuilder.Entity<contacto>()
                .Property(e => e.palabraSecreta)
                .IsUnicode(false);

            modelBuilder.Entity<contacto>()
                .Property(e => e.notificaciones)
                .IsUnicode(false);

            modelBuilder.Entity<datos_vehiculo>()
                .Property(e => e.marca)
                .IsUnicode(false);

            modelBuilder.Entity<datos_vehiculo>()
                .Property(e => e.modelo)
                .IsUnicode(false);

            modelBuilder.Entity<datos_vehiculo>()
                .Property(e => e.color)
                .IsUnicode(false);

            modelBuilder.Entity<datos_vehiculo>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<datos_vehiculo>()
                .Property(e => e.placas)
                .IsUnicode(false);

            modelBuilder.Entity<datos_vehiculo>()
                .Property(e => e.poliza)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.nombreFiscal)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.rfc)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.contacto)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.colonia)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.cp)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.telefono)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.celular)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.ciudad)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.coordenadas)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.css)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.direccionFiscal)
                .IsUnicode(false);

            modelBuilder.Entity<empresa>()
                .Property(e => e.direccionEntrega)
                .IsUnicode(false);

            modelBuilder.Entity<evento>()
                .Property(e => e.descripcion)
                .IsUnicode(false);

            modelBuilder.Entity<evento>()
                .Property(e => e.coordenadas)
                .IsUnicode(false);

            modelBuilder.Entity<evento>()
                .Property(e => e.titulo)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.imeiGPS)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.nombreGPS)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.direccion)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.estatusEntradas)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.numeroChip)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.panicos)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.infoAlertas)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.inicioOperacion)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.finOperacion)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.evento)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.versionSW)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.pass3)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.ipGPS)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.puertoGPS)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.direccionGeo)
                .IsUnicode(false);

            modelBuilder.Entity<gps>()
                .Property(e => e.imeiChip)
                .IsUnicode(false);

            modelBuilder.Entity<grupos>()
                .Property(e => e.nombreGrupo)
                .IsUnicode(false);

            modelBuilder.Entity<grupos>()
                .Property(e => e.inicioOperacion)
                .IsUnicode(false);

            modelBuilder.Entity<grupos>()
                .Property(e => e.finOperacion)
                .IsUnicode(false);

            modelBuilder.Entity<grupos>()
                .Property(e => e.descripcionGrupo)
                .IsUnicode(false);

            modelBuilder.Entity<notificacion_ios>()
                .Property(e => e.datos)
                .IsUnicode(false);

            modelBuilder.Entity<notificacion_ios>()
                .Property(e => e.gcm)
                .IsUnicode(false);

            modelBuilder.Entity<notificacion_modulo>()
                .Property(e => e.via)
                .IsUnicode(false);

            modelBuilder.Entity<notificacion_modulo>()
                .Property(e => e.claveModulo)
                .IsUnicode(false);

            modelBuilder.Entity<notificaciones_usuario>()
                .Property(e => e.correo)
                .IsUnicode(false);

            modelBuilder.Entity<notificaciones_usuario>()
                .Property(e => e.movil)
                .IsUnicode(false);

            modelBuilder.Entity<notificaciones_usuario>()
                .Property(e => e.web)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.nombre)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.apellidoP)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.apellidoM)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.email)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.notas)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.pseudonimo)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.pass)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.palabra_secreta)
                .IsUnicode(false);

            modelBuilder.Entity<usuario>()
                .Property(e => e.telefonoEmergencias)
                .IsUnicode(false);

            modelBuilder.Entity<cola_gps>()
                .Property(e => e.versionSW)
                .IsUnicode(false);

            modelBuilder.Entity<cola_gps>()
                .Property(e => e.cadena)
                .IsUnicode(false);

            modelBuilder.Entity<cola_gps>()
                .Property(e => e.statusEntradas)
                .IsUnicode(false);

            modelBuilder.Entity<cola_gps>()
                .Property(e => e.eventos)
                .IsUnicode(false);

            modelBuilder.Entity<pendientes_cenavit>()
                .Property(e => e.query)
                .IsUnicode(false);

            modelBuilder.Entity<mod_mantenimiento>()
                .Property(e => e.descripcionMantenimiento)
                .IsUnicode(false);
        }
    }
}
