﻿namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.key_maps")]
    public partial class key_maps
    {
        [Key]
        public int id_key{ get; set; }

        [Required]
        [StringLength(100)]
        public string llave{ get; set; }

        [Required]
        public int estatus{ get; set; }
        
        public DateTime? fecha{ get; set; }

        public int contador{ get; set; }
    }
}
