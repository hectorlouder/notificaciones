namespace nucleo_process_main_c.Modelos
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("gps.empresa")]
    public partial class empresa
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public empresa()
        {
            contacto1 = new HashSet<contacto>();
            empresa_gps = new HashSet<empresa_gps>();
            usuario = new HashSet<usuario>();
            grupos = new HashSet<grupos>();
        }

        [Key]
        public int idEmpresa { get; set; }

        [StringLength(60)]
        public string nombreFiscal { get; set; }

        [StringLength(14)]
        public string rfc { get; set; }

        [StringLength(100)]
        public string contacto { get; set; }

        [StringLength(100)]
        public string direccion { get; set; }

        [StringLength(45)]
        public string colonia { get; set; }

        [StringLength(8)]
        public string cp { get; set; }

        [StringLength(20)]
        public string telefono { get; set; }

        [StringLength(20)]
        public string celular { get; set; }

        [StringLength(30)]
        public string ciudad { get; set; }

        public int? estado { get; set; }

        public int? pais { get; set; }

        public byte[] imagen { get; set; }

        public int? zonaHoraria { get; set; }

        public int? nivel { get; set; }

        [StringLength(70)]
        public string coordenadas { get; set; }

        public int? zoom { get; set; }

        [StringLength(200)]
        public string css { get; set; }

        public int? estatus { get; set; }

        [StringLength(45)]
        public string email { get; set; }

        public int? precioEspecial { get; set; }

        [StringLength(80)]
        public string direccionFiscal { get; set; }

        [StringLength(80)]
        public string direccionEntrega { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<contacto> contacto1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<empresa_gps> empresa_gps { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<usuario> usuario { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<grupos> grupos { get; set; }
    }
}
