﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nucleo_process_main_c
{
    public class Procesar
    {
        //Variable para la instancia del form principal
        private Form1 main;

        // Volatile is used as hint to the compiler that this data 
        // member will be accessed by multiple threads. 
        public volatile bool _shouldStop = false;

        //Variable de conexion a la BD del modelado
        public Modelos.Modelos db;

        //Para no estar consultando guardamos un objeto claves
        public List<Modelos.claves> Claves;

        public int caca;

        //Procesar evento
        //public ProcesarEvento ProcesarEvento = new ProcesarEvento();

        /*Proceso principal de realizar el trabajo*/
        public async Task<string> DoWork()
        {
            try{
                while (!_shouldStop){
                    await ProcesarComunicacionAsync();
                    await Task.Delay(1000);
                }
            }catch (Exception ex){
                main.AppendConsola("Error [ProcesarAlertas.Procesar]: " + ex.Message);
                FileLog.Error(ex.Message);
            }

            return "Pues dicen que ya acabe";
        }

        public async Task ProcesarComunicacionAsync(){
            db = new Modelos.Modelos();
            Claves = db.claves.ToList(); //Las claves de error
            
            caca = 15;

            //var comunicaciones = db.cola_gps.Where(x => x.tipo == Properties.Settings.Default.GPSTipo);
            var comunicaciones = db.cola_gps.Where(x => x.estatusProcesamiento == 0).ToList();
            var ProcesarDistanciaRecorrida = new ProcesarDistanciaRecorrida();
            var ProcesarEvento = new ProcesarEvento();//Pasamos la comunicacion para procesarla
            var ProcesarAlertas = new ProcesarAlertas();
            var ProcesarReglas = new ProcesarReglas();
            var ProcesarADC = new ProcesarADC();

            foreach (var comunicacion in comunicaciones){
                var transaction = db.Database.BeginTransaction();
                try{
                    //var r       = await Task.FromResult<bool>(ProcesarEvento.Procesar());
                    await ProcesarAlertas.ProcesarAsync(comunicacion);
                    //await ProcesarADC.Procesar(comunicacion);

                    if (comunicacion.letraTrasmision == "A"){//Trasmicion correcta
                        if (comunicacion.statusMotor) await ProcesarDistanciaRecorrida.Procesar(comunicacion);

                        //Cuidado de las reglas
                        if (comunicacion.gps.estatus == 1){
                            if (comunicacion.statusMotor){
                                await ProcesarReglas.Mantenimiento(comunicacion);
                                await ProcesarReglas.Grupo(comunicacion);
                                await ProcesarReglas.Restricciones(comunicacion);
                            }
                        }

                    }
                    else if (comunicacion.letraTrasmision == "V"){
                        //var r_ = await Task.FromResult<bool>(ProcesarEvento.Procesar());
                    }

                    transaction.Commit();
                }catch (Exception ex) {
                    //transaction.Rollback();
                    main.AppendConsola("Error [ProcesarAlertas.ProcesarComunicacion]: " + ex.Message);
                    FileLog.Error(ex.Message);
                }
            }

            db.Dispose();
        }

        /*Bring la instancia del form1*/
        public void setForm(Form1 f){
            this.main = f;
        }
    }
}
