﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nucleo_process_main_c
{
    class FileLog
    {
        //Log de errores
        public static void Error(string log)
        {
            StreamWriter w = File.AppendText(System.DateTime.Now.ToString("yyyy-MM-dd") + "-Errores.txt");
            w.WriteLine("{0}: {1}", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), log);
            w.Flush();
            w.Close();
        }
    }
}
