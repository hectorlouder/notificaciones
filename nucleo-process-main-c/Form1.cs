﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace nucleo_process_main_c
{
    public partial class Form1 : Form
    {

        Procesar ProcesarInstancia = new Procesar();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e){
            ProcesarInstancia.setForm(this); //Para pasar el form a notificaciones class
            Task.Run(() => this.start()); //Iniciamos el trabajo asycrono
        }

        private async void start(){
            string last_message = await ProcesarInstancia.DoWork();
        }

        /*
         * Funciones consola
         */
        delegate void AppendConsolaCallback(string message);
        public void AppendConsola(string message){
            if (this.notificacionTextBox.InvokeRequired)
            {
                AppendConsolaCallback d = new AppendConsolaCallback(AppendConsola);
                this.Invoke(d, new object[] { message });
            }
            else
            {
                notificacionTextBox.AppendText(message + "\n");
            }
        }

        delegate void CleanConsolaCallback();
        public void CleanConsola(){
            if (this.notificacionTextBox.InvokeRequired)
            {
                CleanConsolaCallback d = new CleanConsolaCallback(CleanConsola);
                this.Invoke(d);
            }
            else
            {
                notificacionTextBox.Clear();
            }
        }
    }
}
