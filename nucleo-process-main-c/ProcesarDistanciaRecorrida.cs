﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nucleo_process_main_c
{
    class ProcesarDistanciaRecorrida : Procesar
    {

        public async Task<bool> Procesar(Modelos.cola_gps comunicacion){
            try{
                /*
                    R = radio de la Tierra
                    Δlat = lat2−lat1
                    Δlong = long2− long1
                    a = sin²(Δlat / 2) + cos(lat1) x cos(lat2) x sin²(Δlong / 2)
                    c = 2 x atan2(√a, √(1−a))
                    d = R x c
                */

                double distancia = 0;
                int r = 6371;
                double distanciaLatitud = 0;
                double distanciaLongitud = 0;

                double latitud_anterior = comunicacion.gps.latitud;
                double longitud_anterior = comunicacion.gps.longitud;

                //Al no tener referencia de la posicion anterior se agregan los valores
                if (latitud_anterior == 0 || longitud_anterior == 0 || (latitud_anterior == comunicacion.latitud && longitud_anterior == comunicacion.longitud) ){
                    comunicacion.gps.latitud  = comunicacion.latitud;
                    comunicacion.gps.longitud = comunicacion.longitud;
                    await db.SaveChangesAsync();

                    return true;
                }

                distanciaLatitud  = (comunicacion.latitud - latitud_anterior) * (Math.PI / 180);
                distanciaLongitud = (comunicacion.longitud - longitud_anterior) * (Math.PI / 180);

                double a = 0;
                a = ( Math.Sin(distanciaLatitud / 2) * Math.Sin(distanciaLatitud/2) ) + Math.Cos(latitud_anterior * (Math.PI / 180) ) * Math.Cos(comunicacion.latitud * (Math.PI / 180) ) * ( Math.Sin(distanciaLongitud / 2) * Math.Sin(distanciaLongitud / 2) );
                double c = 0;
                c = 2 * Math.Atan2( Math.Sqrt(a) , Math.Sqrt(1 - a) );

                distancia = Math.Round(r*c, 2);

                if (comunicacion.velocidad < 100) distancia *= 0.9029;

                //Aumentamos el odometro y cambios el valor de la comunicacion anterior
                comunicacion.gps.acumuladorDistancia += distancia;
                comunicacion.gps.latitud = comunicacion.latitud;
                comunicacion.gps.longitud = comunicacion.longitud;
                await db.SaveChangesAsync();

            }catch (Exception ex){
                FileLog.Error(ex.Message);
                return false;
            }

            return true;
        }
    }
}
