﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nucleo_process_main_c
{
    class ProcesarReglas : Procesar
    {
        public async Task Mantenimiento(Modelos.cola_gps comunicacion)
        {
            try {
                //Si no tiene informacion del modulo cuello
                if (db.mod_mantenimiento.Count(x => x.boolAlerta == false && x.idGPS == comunicacion.idGPS) == 0) return;

                //Clave del evento Matenimiento
                var claves_evento = Claves.Where(x => x.clave == "MMC").Take(1).Single();

                /* 
                 * Revision por Kilometraje
                 */

                //SELECT* FROM mod_mantenimiento where idGPS = '" & idGPS & "' and boolAlerta = '0';
                var mantenimientos = db.mod_mantenimiento.Where(x => x.boolAlerta == false).Where(x => x.idGPS == comunicacion.idGPS).Last();
                decimal? kmMantenimiento = mantenimientos.kilometrajeA ?? 0;
                if (kmMantenimiento > 0){
                    if (comunicacion.gps.acumuladorDistancia > 0){
                        //Al insertar el nuevo evento
                        db.evento.Add(new Modelos.evento{
                            idClave = claves_evento.idClave,
                            idGPS = comunicacion.idGPS,
                            fecha = DateTime.Now,
                            estatus = false,
                            descripcion = "El vehículo " + comunicacion.gps.nombreGPS + " ha sobrepasado el kilometraje para el mantenimiento: " + mantenimientos.descripcionMantenimiento,
                            coordenadas = comunicacion.coordenadas,
                            extra = 0,
                            titulo = claves_evento.titulo
                        });

                        //Update mod_mantenimiento set fecha = NULL, boolAlerta = '0', kilometrajeA = '" & nuevokm & "' where idMantenimiento = '" & idMantenimiento & "'
                        mantenimientos.boolAlerta = true;
                        mantenimientos.fecha = null;
                        mantenimientos.kilometrajeA = mantenimientos.kilometrajeA + mantenimientos.kilometraje;
                        await db.SaveChangesAsync();
                    }
                }

                /* 
                 * Revision por fecha
                 */
                if ( mantenimientos.fecha == DateTime.MinValue && mantenimientos.fecha >= DateTime.Today) {
                    //Al insertar el nuevo evento
                    db.evento.Add(new Modelos.evento{
                        idClave = claves_evento.idClave,
                        idGPS = comunicacion.idGPS,
                        fecha = DateTime.Now,
                        estatus = false,
                        descripcion = "El vehículo " + comunicacion.gps.nombreGPS + " ha sobrepasado la fecha establecida en "+ mantenimientos.fecha +"para el mantenimiento:" + mantenimientos.descripcionMantenimiento,
                        coordenadas = comunicacion.coordenadas,
                        extra = 0,
                        titulo = claves_evento.titulo
                    });
                    mantenimientos.boolAlerta = true;
                    await db.SaveChangesAsync();
                }

            } catch (Exception ex) {
                FileLog.Error(ex.Message);
            }
        }

        public async Task Grupo(Modelos.cola_gps comunicacion)
        {
            try{
                var t = db.empresa.ToList();
                //Select * from grupos_gps natural join grupos where idGPS = idGPS;
                var grupos = from grupos_gps in db.grupos_gps
                             join g in db.grupos on grupos_gps.idGrupo equals g.idGrupo
                             where grupos_gps.idGPS == comunicacion.idGPS
                             select new { grupos_gps, g };

                if (grupos.Count() == 0) return;

                foreach (var grupo in grupos){

                    /*
                     * Exeseso de velocidad
                     */
                    if ( (comunicacion.velocidad > grupo.g.velocidadMaxima) && grupo.g.velocidadMaxima != null){
                        //Al insertar el nuevo evento
                        db.evento.Add(new Modelos.evento{
                            idClave = Claves.Where( x => x.clave == "MGV").Single().idClave,
                            idGPS = comunicacion.idGPS,
                            fecha = DateTime.Now,
                            estatus = false,
                            descripcion = "El vehículo " + comunicacion.gps.nombreGPS + " ha excedido el límite de velocidad establedido en el grupo " + grupo.g.nombreGrupo + " a una velocidad de:" + comunicacion.velocidad,
                            coordenadas = comunicacion.coordenadas,
                            extra = 0,
                            titulo = Claves.Where(x => x.clave == "MGV").Single().titulo
                        });
                        await db.SaveChangesAsync();
                    }

                    /*
                     * Dias de operacion
                     */
                    int d = (int)DateTime.Now.DayOfWeek;

                    if (d == 0) d = 6;
                    else d -= 1;

                    if ( 
                        grupo.g.adiasOperacion[d] == 0 || 
                        (
                            (grupo.g.adiasOperacion[d] == 0 && grupo.g.ainicioOperacion[d] != grupo.g.afinOperacion[d] && grupo.g.afinOperacion[d] > grupo.g.ainicioOperacion[d] ) &&
                            (comunicacion.cadenaFecha < grupo.g.ainicioOperacion[d]) ||
                            (comunicacion.cadenaFecha > grupo.g.afinOperacion[d])
                        )
                    ){
                        //Al insertar el nuevo evento
                        db.evento.Add(new Modelos.evento{
                            idClave = Claves.Where(x => x.clave == "MGH").Single().idClave,
                            idGPS = comunicacion.idGPS,
                            fecha = DateTime.Now,
                            estatus = false,
                            descripcion = "El vehículo " + comunicacion.gps.nombreGPS + " se ha usado en un horario no permitido por el grupo " + grupo.g.nombreGrupo,
                            coordenadas = comunicacion.coordenadas,
                            extra = 0,
                            titulo = Claves.Where(x => x.clave == "MGH").Single().titulo
                        });
                        await db.SaveChangesAsync();
                    }
                }

            }catch (Exception ex){
                FileLog.Error(ex.Message);
            }
        }

        public async Task Restricciones(Modelos.cola_gps comunicacion)
        {

        }
    }
}
