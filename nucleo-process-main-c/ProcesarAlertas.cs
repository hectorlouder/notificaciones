﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nucleo_process_main_c
{
    class ProcesarAlertas : Procesar
    {

        public async Task ProcesarAsync(Modelos.cola_gps comunicacion){
            //Administración de las alertas de pánico (Solo 3 son ligadas a monitoreo)
            try{
                /*
                 * Solo las Alertas ligadas a CENAVIT son del 1 al 3
                 * en adelante solo se crea el evento
                 */
                for (int i = 1; i < 7; i++){
                    if (comunicacion.statusGPS[i] == 1){
                        var c = base.caca;
                        var clave = "P"+i;
                        var descripcion = "Vehículo " + comunicacion.gps.nombreGPS + " activó alerta de pánico "+i;
                        var claves_evento = db.claves.Where(x => x.clave == clave).Take(1).Single();
                        //var claves_evento = Claves.Where(x => x.clave == clave).Take(1).Single();

                        //Al insertar el nuevo evento
                        var nEvento = new Modelos.evento{
                            idClave = claves_evento.idClave,
                            idGPS = comunicacion.idGPS,
                            fecha = DateTime.Now,
                            estatus = false,
                            descripcion = descripcion,
                            coordenadas = comunicacion.coordenadas,
                            extra = 0,
                            titulo = claves_evento.titulo
                        };

                        db.evento.Add(nEvento);
                        await db.SaveChangesAsync();

                        if (i <= 3){
                            if (comunicacion.gps.panicosArray[i] == 1) await PendienteCenavit(comunicacion, clave, nEvento);
                        }
                    }
                }

            }catch (Exception ex){
                FileLog.Error("Error [ProcesarAlertas.Procesar]:" + ex.Message);
            }
        }

        public async Task PendienteCenavit(Modelos.cola_gps comunicacion, string clave, Modelos.evento evento){
            try {
                //Clave de evento generada para los xeventos online de puntoalerta
                var clave_evento = clave + "|" + comunicacion.idGPS;
                string noCliente = "";

                var cliente = from empresa_gps in db.empresa_gps
                              join empresa in db.empresa on empresa_gps.idEmpresa equals empresa.idEmpresa
                              where empresa.nivel == 2
                              where empresa_gps.idGPS == comunicacion.idGPS
                              select empresa;

                if (cliente.Count() > 0) {
                    noCliente = "N" + cliente.Take(1).Single().idEmpresa;
                } else {
                    cliente = from empresa_gps in db.empresa_gps
                              join empresa in db.empresa on empresa_gps.idEmpresa equals empresa.idEmpresa
                              where empresa.nivel == 1
                              where empresa_gps.idGPS == comunicacion.idGPS
                              select empresa;

                    if (cliente.Count() == 0) return; //Si no existe el cliente Salimos del meteodo

                    noCliente = Convert.ToString(cliente.Take(1).Single().idEmpresa);
                }

                db.pendientes_cenavit.Add(new Modelos.pendientes_cenavit {
                    query = "Insert into xeventos_online(no_cliente,fecha_hora,clave_evento,descripcion,receptora,prefijo) values('" + noCliente + "' ,NOW(), '" + clave_evento + "', '" + evento.idClave + "', '17', '3')",
                    estatus = 0,
                    fechaC = DateTime.Now
                });

                await db.SaveChangesAsync();

            }catch (Exception ex){
                FileLog.Error("Error [ProcesarAlertas.PendienteCenavit]:" + ex.Message);
            }
        }

    }
}
